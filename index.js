// STUFF LIKE KATAS1; added here for readability
// num1: 1-20
for(let counter = 1; counter <=20; counter++) {
    let span1 = document.createElement("span");
    let text1 = document.createTextNode(counter + " ");
    span1.appendChild(text1);
    let placeHere1 = document.getElementById("num1");
    placeHere1.appendChild(span1);
};

// num2: 1-20 evens
for(let counter = 1; counter <= 20; counter++) {
    if(counter % 2 == 0) {
        let span2 = document.createElement("span");
        let text2 = document.createTextNode(counter + " ");
        span2.appendChild(text2);
        let placeHere2 = document.getElementById("num2");
        placeHere2.appendChild(span2);
    }
}

// num3: 1-20 odds
for(let counter = 1; counter <= 20; counter++) {
    if(counter % 2 != 0) {
        let span3 = document.createElement("span");
        let text3 = document.createTextNode(counter + " ");
        span3.appendChild(text3);
        let placeHere3 = document.getElementById("num3");
        placeHere3.appendChild(span3);
    }
}

// num4: mult of 5 up to 100
for(let counter = 5; counter <= 100; counter++) {
    if(counter % 5 == 0) {
        let span4 = document.createElement("span");
        let text4 = document.createTextNode(counter + " ");
        span4.appendChild(text4);
        let placeHere4 = document.getElementById("num4");
        placeHere4.appendChild(span4);
    }
};

// num5: squares of 1-10
for(let counter = 1; counter <= 10; counter++) {
    let span5 = document.createElement("span");
    let text5 = document.createTextNode(counter*counter + " ");
    span5.appendChild(text5);
    let placeHere5 = document.getElementById("num5");
    placeHere5.appendChild(span5);
};

// num6: 20-1
for(let counter = 20; counter >= 1; counter--) {
    let span6 = document.createElement("span");
    let text6 = document.createTextNode(counter + " ");
    span6.appendChild(text6);
    let placeHere6 = document.getElementById("num6");
    placeHere6.appendChild(span6);
};

// num7: 20-1 evens
for(let counter = 20; counter >= 1; counter--) {
    if(counter % 2 == 0) {
        let span7 = document.createElement("span");
        let text7 = document.createTextNode(counter + " ");
        span7.appendChild(text7);
        let placeHere7 = document.getElementById("num7");
        placeHere7.appendChild(span7);
    }
}

// num8: 20-1 odds
for(let counter = 20; counter >= 1; counter--) {
    if(counter % 2 != 0) {
        let span8 = document.createElement("span");
        let text8 = document.createTextNode(counter + " ");
        span8.appendChild(text8);
        let placeHere8 = document.getElementById("num8");
        placeHere8.appendChild(span8);
    }
}

// num9: mult of 5 down from 100
for(let counter = 100; counter >= 5; counter--) {
    if(counter % 5 == 0) {
        let span9 = document.createElement("span");
        let text9 = document.createTextNode(counter + " ");
        span9.appendChild(text9);
        let placeHere9 = document.getElementById("num9");
        placeHere9.appendChild(span9);
    }
};

// num10: squares of 10-1
for(let counter = 10; counter >= 1; counter--) {
    let span10 = document.createElement("span");
    let text10 = document.createTextNode(counter*counter + " ");
    span10.appendChild(text10);
    let placeHere10 = document.getElementById("num10");
    placeHere10.appendChild(span10);
}



// USING SAMPLEARRAY
const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472]; 

// num11: all sampleArray
let span11 = document.createElement("span");
let text11 = document.createTextNode(sampleArray.join(", "));
span11.appendChild(text11);
let placeHere11 = document.getElementById("num11");
placeHere11.appendChild(span11);

// num12: sampleArray evens
const evens = sampleArray.filter(q12 => q12 % 2 == 0);
/*
const evens = sampleArray.filter(q12 => {
    return q12 % 2 == 0;
});
*/
let span12 = document.createElement("span");
let text12 = document.createTextNode(evens.join(", "));
span12.appendChild(text12);
let placeHere12 = document.getElementById("num12");
placeHere12.appendChild(span12);

// num13: sA odds
const odds = sampleArray.filter(q13 => q13 % 2 != 0);
let span13 = document.createElement("span");
let text13 = document.createTextNode(odds.join(", "));
span13.appendChild(text13);
let placeHere13 = document.getElementById("num13");
placeHere13.appendChild(span13);

// num14: sA squared
const squares = q14 => q14 * q14;
let span14 = document.createElement("span");
let text14 = document.createTextNode(sampleArray.map(squares).join(", "));
span14.appendChild(text14);
let placeHere14 = document.getElementById("num14");
placeHere14.appendChild(span14)

//num15: sum of 1-20
let sum = 0;
// ^cant be inside for loop b/c let sum = 0 cant be in the loop because you need the value of sum to persist after each loop. if it was inside the loop, after each iteration, the value of loop is lost since its scope would not be outside the loop. thats why on previous assignments you could initial and make elements in a loop. they are made each time through and there is no conflict since they are dropped from memory once you leave the scope of the first iteration
for(let q15 = 1; q15 <=20; q15++) {
    sum = q15 + sum 
    // or sum += q15
};
let span15 = document.createElement("span");
let text15 = document.createTextNode(sum);
span15.appendChild(text15);
let placeHere15 = document.getElementById("num15");
placeHere15.appendChild(span15);

// num16: sum of sA
const adding = sampleArray.reduce((total, current) => total + current);
// const adding = sampleArray.reduce((q16, al) => {
//     return q16 + al
// });
let span16 = document.createElement("span");
let text16 = document.createTextNode(adding);
span16.appendChild(text16);
let placeHere16 = document.getElementById("num16");
placeHere16.appendChild(span16);
/* 
let sum = 0;
for(let nums = 0; nums < sampleArray.length; nums++) {
    sum += sampleArray[nums];
}
*/ 

//num17: min of sA
let span17 = document.createElement("span");
let text17 = document.createTextNode(Math.min(...sampleArray));
span17.appendChild(text17);
let placeHere17 = document.getElementById("num17");
placeHere17.appendChild(span17);

//num18: max of sA
let span18 = document.createElement("span");
let text18 = document.createTextNode(Math.max(...sampleArray));
span18.appendChild(text18);
let placeHere18 = document.getElementById("num18");
placeHere18.appendChild(span18);



// RECTANGLES AND COLORS
// num19: 20rect; 20x100
for(counter=1; counter<=20; counter++) {
    let span19 = document.createElement("div");
    span19.className = "rectangle";
    span19.style.width = "100px";
    span19.style.backgroundColor = "grey";
    let placeHere19 = document.getElementById("num19");
    placeHere19.appendChild(span19);
}

// num20: 20rect; 20x105^+5
for(counter=1; counter<=20; counter++) {
    let span20 = document.createElement("div");
    span20.className = "rectangle";
    span20.style.width = (100 + (counter*5)) + "px";
    span20.style.backgroundColor = "grey";
    let placeHere20 = document.getElementById("num20");
    placeHere20.appendChild(span20);
}

// num21: 20rect; 20xsA
// because it is an array, it starts at 0 and ends at 19; could probs do tostring or w/e, but this is better for now
for(counter=0; counter<=19; counter++) {
    let span21 = document.createElement("div");
    span21.className = "rectangle";
    span21.style.width = sampleArray[counter] + "px";
    span21.style.backgroundColor = "grey";
    let placeHere21 = document.getElementById("num21");
    placeHere21.appendChild(span21);
}

// num22: 20rect; 20xsA; everyother red
for(counter=0; counter<=19; counter++) {
    let span22 = document.createElement("div");
    span22.className = "rectangle";
    span22.style.width = sampleArray[counter] + "px";
    if(counter % 2 == 0) {
        span22.style.backgroundColor = "grey";
    } else {
        // ^ this also works: if(counter % 2 != 0) 
        span22.style.backgroundColor = "red";
    }
    let placeHere22 = document.getElementById("num22");
    placeHere22.appendChild(span22);
}

// num23: 20rect; 20xsA; even widths red
for(counter=0; counter<=19; counter++) {
    let span23 = document.createElement("div");
    span23.className = "rectangle";
    span23.style.width = sampleArray[counter] + "px";
    if(sampleArray[counter] % 2 == 0) {
        span23.style.backgroundColor = "red";
    } else {
        // ^ this also works: if(sampleArray[counter] % 2 != 0) 
        span23.style.backgroundColor = "grey";
    }
    let placeHere23 = document.getElementById("num23");
    placeHere23.appendChild(span23);
}